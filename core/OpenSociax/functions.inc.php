<?php
/**
 * Created by PhpStorm.
 * User: changwei
 * Date: 17/5/31
 * Time: 下午12:01
 */
/* L 函数用于读取/设置语言配置
 * @param $key 配置名称
 * @param array $data 配置值
 * @return mixed|string
 */
function L($key,$data = array())
{
    $key = strtoupper($key);
    if (!isset($GLOBALS['_lang'][$key])) {
        return $key;
    }
    if (empty($data)) {
        return $GLOBALS['_lang'][$key];
    }
    $replace = array_keys($data);
    foreach ($replace as $k => $v) {
        $v = '{'.$v.'}';
    }
    return str_replace($replace,$data,$GLOBALS['_lang'][$key]);
}

/**
 * C函数用于读取/设置系统配置
 * @param string $name
 * @param string $value
 * @return maxed 配置值|设置状态
 */
function C($name = null,$value = null)
{
    global $ts;
    //无参数时获取所有
    if (empty($name)) return $ts['_config'];

    //优先执行设置获取或者赋值
    if (is_string($name))
    {
        if (!strpos($name,'.')) {
            $name = strtolower($name);
            if (is_null($value)) {
                return isset($ts['_config'][$name]) ? $ts['_config'][$name] : null;
            }
            $ts['_config'][$name] = $value;
            return;
        }

        $name = explode('.',$name);
        $name[0] = strtolower($name[0]);
        if (is_null($value)) {
            return isset($ts['_config'][$name[0]][$name[1]]) ? $ts['_config'][$name[0]][$name[1]] : null;
        }
        $ts['_config'][$name[0]][$name[1]] = $value;
        return;
    }

    //批量设置
    if (is_array($name)) {
        //array_change_key_case($array,CASE_UPPER)将数组的所有的键转换为大写字母
        //array_change_key_case($array,CASE_LOWER)将数组所有建转换为小写字母
        //默认转换成大写
        return $ts['_config'] = array_merge((array)$ts['_config'],array_change_key_case($name));
    }
    return null;
}