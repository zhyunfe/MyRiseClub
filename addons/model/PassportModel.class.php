<?php
/**
 * 通行证模型 --  业务逻辑模型
 * Created by PhpStorm.
 * User: Demoer
 * Date: 17/5/31
 * Time: 下午1:06
 */
class PassportModel {
    protected $error = null;//错误信息
    protected $success = null;//成功信息
    protected $rel = array();//判断是否第一次登陆

    /**
     * 返回最后的错误信息
     * @return string 最后的错误信息
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * 返回最后的正确信息
     * @return string 正确信息
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * 验证后台登陆
     * @return bool
     */
    public function checkAdminLogin()
    {
        if ($_SESSION['adminLogin']) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 登陆后台
     * @return bool
     */
    public function adminLogin()
    {
        if (is_numeric($_POST['uid'])) {
            $map['uid'] = intval($_POST['uid']);
        } else {
            $uname = t($_POST['uname']);
            $map = "(login='{$uname}' OR uname = '{$uname}')";
        }

        $login = M('user')->where($map)->find();
        if ($this->loginLocal($login['login'],$_POST['password'])) {
            $GLOBALS['ts']['mid'] = $_SESSION['adminLogin'] = intval($_SESSION['mid']);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 退出后台
     */
    public function adminLogout()
    {
        unset($_SESSION['adminLogin']);
        session_destroy($_SESSION['adminLogin']);
    }

    /**
     * 使用本地账号登录 (密码为null时不参与验证)
     * @param $login 登录名称 邮箱或者用户名
     * @param null $password 密码
     * @param bool $is_remember_me 是否记录登录状态,默认为false
     * @param null $uid 用户id
     * @return bool 是否登录成功
     */
    public function loginLocal($login,$password = null,$is_remember_me = false,$uid = null)
    {
        $res = false;
        if (UC_SYNC) {
            $res = $this->ucLogin($login,$password,$is_remember_me);
            if ($res) {
                return true;
            }
        }
        //记录密码
        if ($is_remember_me) {
            $expire = 3600 * 24 * 7;
            cookie('TSV3_LOGGED_EMAIL',$this->jiami(C('SECURE_CODE').".{$login}"),$expire);
            cookie('TSV3_LOGGED_PASS',$this->jiami(C('SECURE_CODE').".{password}"),$expire);
        } else {
            cookie('TSV3_LOGGET_EMAIL',NULL);
            cookie('TSV3_LOGGET_PASS',NULL);
        }
        if (!$password && $uid) {
            $user = model('User')->where("uid=$uid")->find();
        } else {
            $user = $this->getLocalUser($login,$password);
        }

        return $user['uid']>0 ? $this->_recordLogin($user['uid'],$is_remember_me) : false;
    }
    private function ucLogin($username,$password,$is_remember_me)
    {
        //载入UC客户端SDK
        include once SITE_PATH.'/api/uc_client/client.php';

    }
    public function getLocalUser($login,$password)
    {
        $login = addslashes($login);
        $password = addslashes($password);

        if (empty($login) || empty($password)) {
            $this->error = L('PUBLIC_ACCOUNT_EMPTY');//账户或者密码不能为空
            return false;
        }

        if ($this->isValidEmail($login)) {
            $map = "(login = '{$login}' or email = '{$login}') AND is_del = 0";
        } else {
            $map = "(login = '{$login}' or uname = '{$login}' AND is_del = 0)";
        }

        $user = model('User')->where(array('login'=>$login,'is_del'=>0))->find();
        if (!$user) {
            $user = model('User')->where(array('uname'=>$login,'is_del'=>0))->find();
        }

        if (!$user) {
            if ($this->isValidPhone($login)) {
                $uid = D('user_tel_bind')->where(array('tel'=>$login))->getField('uid');
            }
        }
    }
    /**
     * 加密函数
     * @param $txt 需要加密的字符串
     * @param null $key 加密密钥,默认读取SECURE_CODE配置
     * @return string 加密后的字符串
     */
    private function jiami($txt,$key = null)
    {
        empty($key) && $key = C('SECURE_CODE');

        //有mcrypt扩展时
        if (function_exists('mcrypt_module_open')) {
            return desencrypt($txt,$key);
        }

        //无mcrypt扩展时
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-=_";
        $nh = rand(0,64);
        $ch = $chars[$nh];
        $mdKey = md5($key,$ch);
        $mdKey = substr($mdKey.$nh % 8,$nh % 8 + 7);
        $txt = base64_encode($txt);
        $tmp = '';
        $i = 0;
        $j = 0;
        $k = 0;
        for ($i = 0; $i < strlen($txt); $i++) {
            $k = $k == strlen($mdKey) ? 0 : $k;
            $j = ($nh + strpos($chars,$txt[$i]) + ord($mdKey[$k++])) % 64;
            $tmp .= $chars[$j];
        }
        return $ch.$tmp;
    }
    /**
     * 解密函数
     * @param string $txt 待解密的字符串
     * @param string $key 解密密钥，默认读取SECURE_CODE配置
     * @return string 解密后的字符串
     */
    private function jiemi($txt, $key = null) {
        empty($key) && $key = C('SECURE_CODE');
        //有mcrypt扩展时
        if(function_exists('mcrypt_module_open')){
            return desdecrypt($txt, $key);
        }
        //无mcrypt扩展时
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-=_";
        $ch = $txt[0];
        $nh = strpos($chars, $ch);
        $mdKey = md5($key.$ch);
        $mdKey = substr($mdKey, $nh % 8, $nh % 8 + 7);
        $txt = substr($txt, 1);
        $tmp = '';
        $i = 0;
        $j = 0;
        $k = 0;
        for($i = 0; $i < strlen($txt); $i++) {
            $k = $k == strlen($mdKey) ? 0 : $k;
            $j = strpos($chars, $txt[$i]) - $nh - ord($mdKey[$k++]);
            while($j < 0) {
                $j += 64;
            }
            $tmp .= $chars[$j];
        }
        return base64_decode($tmp);
    }
}
