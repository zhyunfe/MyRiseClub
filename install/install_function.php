<?php
/**
 * Created by PhpStorm.
 * User: Demoer
 * Date: 17/5/27
 * Time: 下午4:42
 */
if (!defined('THINKSNS_INSTALL')) {
    exit('Access Denied');
}

function str_parcate($cate,&$str_parcate,$startID = 0)
{
    if (!$cate[$startID]['cup']) return;
    foreach ($cate as $key => $value)
    {
        if ($value['cup'] == $startID)
        {
            $str_parcate .= $value['cid'].',';
            $str_parcate($cate,$str_parcate,$value['cid']);
        }
    }
}