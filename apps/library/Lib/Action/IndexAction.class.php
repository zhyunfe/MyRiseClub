<?php
/**
 * Created by PhpStorm.
 * User: changwei
 * Date: 17/6/14
 * Time: 下午3:16
 */
class IndexAction extends Action {
    protected  $LIBRARY_KEY;
    protected $avatarModel;
    protected $coonection_Inf;
    protected $connection_Club = array(
        'DB_TYPE'=>'mysql',
        'DB_USER'=>'root',
        'DB_PWD' => 'RiseClubdb01123#$%',
        'DB_HOST' => '123.59.147.57',
        'DB_PORT' => '9003',
        'DB_NAME' => 'riseclub'
    );
    public function __construct()
    {
        $this->coonection_Inf = include SITE_PATH . "/config/db_sync_config.inc.php";
        $this->avatarModel = new AvatarModel();
        $this->LIBRARY_KEY = 'd521f765a49c72507257a2620612ee96';
    }
    protected $pageCount = 500;

    /**
     * 初始化控制器,实例化用户档案模型对象
     */
    protected function _initialize()
    {
        $this->avatarModel = new AvatarModel();
        $this->LIBRARY_KEY = "d521f765a49c72507257a2620612ee96";
    }

    public function sync2middle($beginTime,$uid)
    {
        //查询出要保存到中间库的数据
        logger2(0,"Sync2Library start:".date('Y-m-d H:i:s'));
        try{
            //设置当前时间减一天,到当前时间的范围
            if ($beginTime) {
                $mystr = explode('-',$beginTime);
                $startTime = date('Y-m-d',mktime(0,0,0,$mystr[1],$mystr[2],$mystr[0]));
            } else {
                $startTime = date('Y-m-d H:i:s',strtotime("-1 day"));
            }
            $db = new SimpleDB($this->coonection_Club);
            $countSQL = "select COUNT(1) as userCount from ts_user t where t.last_update_time between unix_timestamp('".$startTime."') and unix_timestamp(now())";
            if (!empty($uid)) {
                $countSQL .= "and uid = ".$uid;
            }
            $tsUserCountResult = $db->query($countSQL);
            $tsUserCount = $tsUserCountResult[0]['userCount'];
            $tsUserListSQL = "select t.uid,t.uname,t.login_salt as user_num,t.email from ts_user t where t.last_update_time between unix_timestamp('".$startTime."') and unix_timestamp(now())";
            if (!empty($uid)) {
                $tsUserListSQL .= 'and t.uid = '.$uid;
            }
            dump('pageCount = '. $this->pageCount);
            if ($tsUserCount > $this->pageCount) {
                $tsUsernum = ceil($tsUserCount/$this->pageCount);
                for($i = 0;$i<$tsUsernum;$i++) {
                    $startRow = $i * $this->pageCount;
                    $tsUserList = $db->query($tsUserListSQL . "LIMIT ". $startRow . "," . $this->pageCount);
                    if (!empty($tsUserList)) {
                        $this->processUser($db,$tsUserList);
                        unset($tsUserList);
                    }
                    break;
                }
            } else {
                //一次性处理数据
                $tsUserList = $db->query($tsUserListSQL);
                if (!empty($tsUserList)) {
                    $this->processUser($db,$tsUserList);
                    unset($tsUserList);
                }
            }
            $endTime = "Sync2Library end :".date("Y-m-d H:i:s");
            logger2(0,"Process Info Count：[".$tsUserCount."]");
            logger2(0,$endTime);
            $this->ajaxReturn(null, "sync2middle success ".$endTime, 1);
        }catch (Exception $ex){
            $endTime = "Sync2Library end:".date('Y-m-d H:i:s');
            $this->ajaxReturn(null,"sync2middle fail".$endTime,0);
        }
    }
    public function processUser($db,$tsUserList)
    {
        $uidStr = $this->userInfoArrayToUidStr($tsUserList);
        $studentInfoSQL = "select t1.uid,(case when t1.phase = 'PreK' then 'K'
                                               when t1.phase = 'PreMiddle' then 'S5'
                                               when t1.phase = 'RiseUp' then 'S5'
                                               else t1.phase end) as level,
                                               t1.mdate from ts_data_student as t1 left join ts_course_phase as p1
                                               on t1.phase = p1.phase_name where t1.uid in (".$uidStr.")order by p1.phase_num";
        $studentInfo = $db->query($studentInfoSQL);

        //将查询结果封装进数组方便使用UID 进行提取
        $studentInfoArray = array();
        for ($i = 0;$i < count($studentInfo);$i ++) {
            $item = $studentInfo[$i];
            $studentInfoArray[$item['uid']] = $item['LEVEL'];
        }
        //循环所有学员数据
        for ($i = 0;$i < count($tsUserList); $i++) {
            $item = $tsUserList[$i];
            $userId = $item['uid'];
            $temp_user_image = $this->avatarModel->getUserAvatarByUid ( $userId );
            $temp_image_url = str_replace ( "/data/wwwroot/", "", $temp_user_image ['avatar_small'] );
            $tsUserList [$i] ['user_img'] = $temp_image_url;
            $phase = $studentInfoArray[$userId];
            // 如果没有学员数据则所有用户都是非学员数据
            if(empty($studentInfoArray)){
                $tsUserList [$i] ['LEVEL'] = 'Trial';
            }else{
                if (empty($phase)) {
                    $tsUserList [$i] ['LEVEL'] = 'Trial';
                } else {
                    $tsUserList [$i] ['LEVEL'] = $phase;
                }
            }
            // 处理用户对应角色
            $temp_role = $db->query("SELECT GROUP_CONCAT(user_group_id) AS user_group_id FROM ts_user_group_link where uid = " . $userId);
            $groupId = $temp_role[0]['user_group_id'];
            if ((strpos($groupId, '17') !== false) || (strpos($groupId, '21') !== false) || (strpos($groupId, '29') !== false)) {
                $tsUserList [$i] ['role'] = 1;
            } else if (strpos($groupId, '19') !== false) {
                $tsUserList [$i] ['role'] = 2;
            } else {
                $tsUserList [$i] ['role'] = 3;
            }
        }
        // 数据保存到中间表
        dump("count(tsUserList)  = " . count($tsUserList));
        $this->saveAll ( $tsUserList );
        unset ( $tsUserList );
    }
    public function userInfoArrayToUidStr($userInfoList)
    {
        $uidStr = '';
        for($i = 0;$i < count($userInfoList); $i++) {
            $item = $userInfoList[$i];
            if ($i==0)
                $uidStr .= $item['uid'];
            else
                $uidStr .= ",".$item['uid'];
        }
        return $uidStr;
    }
}