<?php
/**
 * ThinkSNS Action控制器基类
 * Created by PhpStorm.
 * User: Demoer
 * Date: 17/5/31
 * Time: 上午9:18
 */
abstract class Action
{
    private  $name = '';
    protected  $trace = array();
    protected  $tVar = array();
    protected  $templateFile = '';
    protected  $appCssList  = array();
    protected  $appJsList = array();
    protected  $langJsList = array();

    protected  $site = array();
    protected  $user = array();
    protected  $app = array();
    protected  $mid = 0;
    protected  $uid = 0;

    /**
     * Action constructor.
     * 架构函数 取得模板对
     */
    public function __construct()
    {

    }

    /**
     * 站点信息初始化
     */
    private function initSite()
    {

    }

    /**
     * 应用信息初始化
     */
    private function initApp()
    {

    }

    /**
     * 用户信息初始化
     */
    private function initUser()
    {

    }
}