<?php
/**
 * Created by PhpStorm.
 * User: Demoer
 * Date: 17/5/27
 * Time: 下午4:21
 */
error_reporting(0);
//FILE--/usr/local/var/www/mythink/install/install.php
//dirname(__FILE__)---/usr/local/var/www/mythink/install
define('THINKSNS_INSTALL',TRUE);
define('THINKSNS_ROOT',str_replace('\\','/',substr(dirname(__FILE__),0,-7)));

//设置session路径
//如果session是以file文件存储的话就将session的存放地址放到data/session里面
if (strtolower(@ini_get("session.save_handler")) == 'file') {
    $session_dir = THINKSNS_ROOT.'/data/session';
    if (!is_dir($session_dir))
        mkdir($session_dir,0777,true);
        @session_save_path($session_dir);
}
session_start();

$_TSVERSION = '3.1';

include 'install_function.php';
include 'install_lang.php';