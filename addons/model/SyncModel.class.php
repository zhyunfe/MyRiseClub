<?php

/**
 * Created by PhpStorm.
 * User: changwei
 * Date: 17/6/14
 * Time: 上午10:19
 */
//require(SITE_PATH ./'addons/library/SimpleDB.class.php');
class SyncModel extends Model
{
    protected $connection;
    public function __construct($name)
    {

    }
    /***************每日同步********************/
    //同步城市\学校\班级
    public function city_school_class($starttime)
    {
        set_time_limit(0);
        $db = new SimpleDB($this->connection);
        if ($starttime) {
            $mystr = explode('-',$starttime);
            $now_date = date('Y-m-d',mktime(0,0,0,$mystr[1],$mystr[2],$mystr[0]));
        } else {
            $now_date = date('Y-m-d',strtotime("-1 day"));
        }
        $log['type'] = 0;
        $log['ctime'] = $now_date;
        $log['status'] = 1;
        $log['des'] = 'city_school_class 已经执行';
        D('data_sync_log')->add($log);

        //同步城市
        /**
         * localorg 表
         * 1.从表中找出规定时间内的所有的城市
         * 2.格式化查询出来的城市名称
         * 3.data_city表中是否有该城市,没有的话获取最大城市编号,如果小于2000,那么从2001开始,如果最大值大于2000,那么就递增
         * 4.保存日志
         */
        if ($starttime) {
            $citys = $db->query("SELECT name,_MASK_TO_V2 FROM localorg where
                          type=30 And DATE_FORMAT(createtime,'%Y-%m-%d') >=
                                      DATE_FORMAT('".$now_date."','%Y-%m-%d')
                                  AND name is not null order by _MASK_TO_V2 asc");
        } else {
            $citys = $db->query("SELECT name,_MASK_TO_V2 FROM localorg where type=30
                                AND createtime like '".$now_date."%'
                                AND name is not null order by _MASK_TO_V2 asc");
        }

        if (count($citys)) {
            foreach ($citys as $key=>$value) {
                $city_name = str_replace('分公司','',$value['name']);
                if ($start1 = strpos($city_name,'(')) {
                    $city_name = substr($city_name,0,$start1);
                }
                if ($start2 = strpos($city_name,'(')){
                    $city_name = substr($city_name,0,$start2);
                }

                if (!D('data_city')->where(array('city_name'=>$city_name))->count()){
                    $map1['title'] = array('like',$value['name'].'%');
                    $map1['pid']   = array('gt',0);
                    if ($area_id = D('area')->where($map1)->getField('area_id')) {
                        $data1['city_id'] = $area_id;
                    } else {
                        $max_city_id = intval(D('data_city')->max('city_id'));
                        if ($max_city_id < 2000) {
                            $data1['city_id'] = 2001;
                        } else {
                            $data1['city_id'] = $max_city_id + 1;
                        }
                    }
                    $data1['city_name'] = $city_name;
                    $data1['first_letter'] = getFirstLetter($city_name);
                    $data1['mtime'] = time();
                    $data1['mdate'] = date('Y-m-d H:i:s',$data1['mtime']);

                    $res = D('data_city')->add($data1);
                    $log1['type'] = 1;
                    $log1['ctime'] = $now_date;
                    $log1['_MASK_TO_V2'] = $value['_MASK_TO_V2'];

                    if ($res) {
                        $log1['status'] = 1;
                        $log1['des'] = '城市'.$city_name.'添加成功';
                    } else {
                        $log1['status'] = 0;
                        $log1['des'] = '城市'.$city_name.'添加失败';
                    }
                    D('data_sync_log')->add($log1);
                    unset($log1);
                }
            }
            unset($citys);
        }
        /**
         * localorg表
         * 1.获取到所有的校区的pzj,orgzj,name,iscooperate,_MASK_TO_V2
         * 2.通过school_code 获取school_id判断data_school表里是否有该校区,存在的话就跟新校区的名字
         * 3.通过school_name 获取是否有该校区名称,有的话更新school_code
         * 4.都不符合,就添加新校区,查找校区城市\相关信息,保存
         * 5.保存日志
         */
        //同步校区
        if ($starttime) {
            $schools = $db->query("SELECT pzj,orgzj,name,iscooperate,_MASK_TO_V2 FROM localorg WHERE
                                    type=50 AND DATE_FORMAT(createtime,'%Y-%m-%d') >=
                                    DATE_FORMAT('".$now_date."','%Y-%m-%d')
                                    AND orgzj is not null
                                    AND name is not null order by _MASK_TO_V2 asc");
        } else {
            $schools = $db->query("SELECT pzj,orgzj,name,iscooperate,_MASK_TO_V2 FROM localorg where
								type=50 and createtime like '".$now_date."%'
								and orgzj is not null
								and name is not null order by _MASK_TO_V2 asc");
        }
        if (count($schools)) {
            foreach ($schools as $key => $value) {
                $log2['type'] = 2;
                $log2['ctime'] = $now_date;
                $log2['_MASK_TO_V2'] = $value['_MASK_TO_V2'];
                $save2['mtime'] = $date2['mtime'] = time();
                $save2['mdate'] = $data2['mdate'] = date('Y-m-d H:i:s',$save2['mtime']);
                $save2['sync'] = $date2['sync'] = 1;
                //判断是直营还是合作
                if ('100' == $value['iscooperate']) {
                    $date2['type'] = 1;
                } else {
                    $date2['type'] = 2;
                }
                if ($school_id = D('data_school')->where(array('school_code'=>$value['orgzj']))->getField('school_id')) {
                   //存在该校区,更新校区名称
                    $origin_school_name = D('data_school')->where(array('school_code'=>$value['orgzj']))->getField('school_name');
                    $save2['school_name'] = $value['name'];
                    $res = D('data_school')->where('school_id='.$school_id)->save($save2);
                    if ($res) {
                        $log2['status'] = 1;
                        $log2['des'] = '校区'.$value['name'].'校名更新成功-原校名('.$origin_school_name.')';
                    } else {
                        $log2['status'] = 1;
                        $log2['des'] = '校区'.$value['name'].'校名更新失败-原校名('.$origin_school_name.')';
                    }
                    D('data_sync_log')->add($log2);
                    continue;
                } else if($school_id = D('data_school')->where(['school_name'=>$value['name']])->getField('school_id')) {
                    //校区在前台已经被导入(校区名称相同)
                    $save2['school_code'] = $value['orgzj'];
                    $res = D('data_school')->where('school_id='.$school_id)->save($save2);
                    if (false !== $res) {
                        $log2['status'] = 1;
                        $log2['des'] = '校区“'.$value['name'].'”,school_code更新成功(前台导入)';
                    } else {
                        $log2['status'] = 0;
                        $log2['des'] = '校区“'.$value['name'].'”,school_code更新失败(前台导入)';
                    }
                    D('data_sync_log')->add($log2);
                    continue;
                } else {
                    //添加新校区
                    $data2['school_code'] = $value['orgzj'];
                    $data2['school_name'] = $value['name'];
                    //所属城市
                    $center = $db->query("SELECT `pzj` FROM localorg where `orgzj`='".$value['pzj']."' and `type`=40");	//获取校区所在组织的pzj
                    $localorg_city = $db->query("SELECT `name`,`zj` FROM localorg where `zj`='".$center[0]['pzj']."' and `type`=30");
                    $city_name = $localorg_city[0]['name'];
                    if($city_name){
                        //处理城市名称
                        $city_name = str_replace('分公司', '', $city_name);
                        if($start1 = strpos($city_name, '(')){
                            $city_name = substr($city_name, 0, $start1);
                        }
                        if($start2 = strpos($city_name, '（')){
                            $city_name = substr($city_name, 0, $start2);
                        }

                        $data2['city_id'] = D('data_city')->where(array('city_name'=>$city_name))->getField('city_id');
                        if(!$data2['city_id']){	//ts_data_city表中不存在该城市名称，则添加
                            $c['city_name'] = $city_name;
                            $c['first_letter'] = first_letter($city_name);
                            $c['mtime'] = time();
                            $c['mdate'] = date('Y-m-d',$c['mtime']);
                            $c['zj'] = $localorg_city[0]['zj'];
                            $res = D('data_city')->add($c);
                            if(!$res){
                                $log2['status'] = 0;
                                $log2['des'] = '校区“'.$value['name'].'”，在ts_data_city表中无法找到相应城市“'.$city_name.'”，且该城市添加ts_data_city表失败';
                                D('data_sync_log')->add($log2);
                                continue;
                            }
                        }
                        //添加校区
                        $data2['city_name'] = $city_name;
                        $data2['first_letter'] = getFirstLetter($value['name']);
                        $res = D('data_school')->add($data2);
                        if($res){
                            $log2['status'] = 1;
                            $log2['des'] = '校区“'.$value['name'].'”添加成功';
                        }else{
                            $log2['status'] = 0;
                            $log2['des'] = '校区“'.$value['name'].'”添加失败';
                        }
                        D('data_sync_log')->add($log2);
                        continue;
                    }else{	//查不到城市信息
                        $log2['status'] = 0;
                        $log2['des'] = '校区“'.$value['name'].'”，在localorg表中无法找到相应城市';
                        D('data_sync_log')->add($log2);
                    }
                }
            }
        }
        //同步班级
        //rc_clazz  data_school  data_classroom
        /**
         * 1.查找所有班级
         * 2.判断班级是否存在,存在-更新,不存在-添加
         */
        $today_date = data('Y-m-d',time());
        if ($starttime) {
            $classrooms = $db->query("SELECT * FROM rc_clazz where `校区` != '北京王府井校区'
                                      and (DATA_FORMAT(`开班时间`,'%Y-%m-%d') >=
                                      DATE_FORMAT('".$now_date."','%Y-%m-%d')
                                      or DATE_FORMAT(`结课时间`,'%Y-%m-%d') >=
                                      DATE_FORMAT('".$now_date."','%Y-%m-%d')
                                      or DATE_FORMAT(`updatetime`,'%Y-%m-%d') >=
                                      DATE_FORMAT('".$now_date."','%Y-%m-%d'))
                                      order by `_MASK_TO_V2` asc");
        } else {
            $classrooms = $db->query("SELECT * FROM rc_clazz where `校区`!='北京王府井校区'
                                      and (`开班时间` like '".$today_date."%'
                                      or `结课时间` like '".$now_date."%'
                                      or `updatetime` like '".$now_date."%')
									 order by `_MASK_TO_V2` asc");
        }
        if (count($classrooms)) {
            foreach ($classrooms as $key => $value) {
                $log6['type'] = 6;
                $log6['ctime'] = $now_date;
                $log6['_MASK_TO_V2'] = $value['_MASK_TO_V2'];
                $addData['zj'] = $value['zj'];
                $school = D('data_school')->where(['school_code'=>t($value['中心编码'])])->field('city_id,school_id,school_name')->find();
                if (!$school) {
                    $log6['des'] = 0;
                    $log6['des'] = '班级“'.$value['班号'].'”，在data_school表中无法找到相应校区编码'.t($value['中心编码']).'zj:'.$value['zj'];
                    D('data_sync_log')->add($log6);
                    continue;
                }
                $addData['city_id'] = $school['city_id'];
                $addData['school_id'] = $school['school_id'];
                $addData['class_name'] = $value['课程名称'];
                $addData['stime'] = $value['开班时间'];
                $addData['etime'] = $value['结课时间'];
                $addData['classroom_name'] = $value['班号'];
                $addData['phase'] = $value['课程阶段标识'];
                $addData['status'] = $value['班级状态'];
                $addData['danju'] = $value['单据状态'];
                $addData['zhouqi'] = $value['上课周期'];
                $addData['content'] = $value['教学内容'];
                $addData['target'] = $value['教学目标'];
                $addData['yishang'] = $value['已上课时'];
                $addData['shengyu'] = $value['剩余课时'];
                $addData['first_letter'] = getFirstLetter($value['班号']);
                $addData['sync'] = 1;
                $addData['mtime'] = time();
                $addData['mdate'] = date('Y-m-d H:i:s',$addData['mtime']);
                //判断班级是否存在
                $classroom_id = D('data_classroom')->where(array('zj'=>$value['zj']))->getField('classroom_id');
                if ($classroom_id) {
                    //如果存在班级,更新
                    $res = D('data_classroom')->where(['classroom_id='.$classroom_id])->save($addData);
                    if ($res) {
                        $log6['status'] = 1;
                        $log6['des'] = '班级“'.$value['班号'].'”('.$school['school_name'].'),ID:'.$classroom_id.'更新成功';
                    } else {
                        $log6['status'] = 0;
                        $log6['des'] = '班级“'.$value['班号'].'”('.$school['school_name'].'),ID:'.$classroom_id.'更新失败';
                    }
                    D('data_sync_log')->add($log6);
                    continue;
                } else {
                    //不存在班级,添加
                    $classroom_id = D('data_classroom')->where(array('school_id'=>$school['school_id'],'classroom_name'=>$value['班号']))->getField('classroom_id');
                    if($classroom_id){
                        $res = D('data_classroom')->where('classroom_id='.$classroom_id)->save($addData);
                        if($res !== false){
                            $log6['status'] = 1;
                            $log6['des'] = '班级“'.$v6['班号'].'”('.$school['school_name'].'),ID:'.$classroom_id.'更新成功,(前台导入)';
                        }else{
                            $log6['status'] = 0;
                            $log6['des'] = '班级“'.$v6['班号'].'”('.$school['school_name'].'),ID:'.$classroom_id.'更新失败,(前台导入)';
                        }
                        D('data_sync_log')->add($log6);
                        continue;
                    }else{
                        if($classroom_id = D('data_classroom')->add($addData)){
                            $log6['status'] = 1;
                            $log6['des'] = '班级“'.$v6['班号'].'”('.$school['school_name'].'),ID:'.$classroom_id.'添加成功';
                        }else{
                            $log6['status'] = 0;
                            $log6['des'] = '班级“'.$v6['班号'].'”('.$school['school_name'].'),ID:'.$classroom_id.'添加失败';
                        }
                        D('data_sync_log')->add($log6);
                        continue;
                    }
                }
                unset($addData);
            }
            var_dump("finish city_school_class refresh");
        } else {
            var_dump('no class refresh');
        }
    }

    //更新教师,只添加,不删除
    public function teacher($teacher_jobnumber)
    {
        set_time_limit(0);
        ini_set('memory_limit','5000M');
        ini_set('max_execution_time',0);
        $db = new SimpleDB($this->connection);
        $now_date = date('Y-m-d',strtotime("-1 day"));

        $log['type'] = 0;
        $log['ctime'] = $now_date;
        $log['status'] = 1;
        $log['des'] = 'teacher已执行';
        D('data_sync_log')->add($log);

        //统计
        $statistics = array();

        if ($teacher_jobnumber) {
            $sql = "SELECT a.*,b.orgzj FROM rc_teacher a
                    LEFT JOIN localorg b
                    ON a.`校区` = b.`name`
                    WHERE `员工工号` is not NULL
                    AND `校区` != '北京王府井校区'
                    AND `员工工号`=$teacher_jobnumber";
            $teachers = $db->query($sql);
        } else {
            $teachers = $db->query("SELECT a.*,b.orgzj FROM rc_teacher a
                    LEFT JOIN localorg b
                    ON a.`校区` = b.`name`
                    WHERE `员工工号` is not NULL
                    AND `校区` != '北京王府井校区'");
        }

        if (count($teachers)) {
            foreach ($teachers as $key=>$value) {
                $log3['type'] = 1;
                $log3['ctime'] = $now_date;
                $log3['_MASK_TO_V2'] = $value['_MASK_TO_V2'];
                $school_detail = D('data_school')->where(['school_code'=>$value['orgzj']])->find();
                $addData3['city_id'] = $school_detail['city_id'];
                $addData3['school_id'] = $school_detail['school_id'];
                $addData3['classroom_id'] = D('data_classroom')->where(['school_id'=>$school_detail['school_id'],'classroom_name'=>$value['班级编号']])->getField('classroom_id');
                $addData3['jobnumber'] = $value['员工工号'];
                $teacher_uid = 0;
                if ($teacher_jobnumber) {
                    //从表中删除老师
                    $teacher_uid = D('data_role')->where(['jobnumber'=>$teacher_jobnumber])->getField('uid');
                    if (!$teacher_uid) {
                        $where['uname'] = array('like','%'.$teacher_jobnumber);
                        $map['_complex'] = $where;
                        $temp_user = D('User')->where($map)->find();
                        $teacher_uid = $temp_user['uid'];
                    }
                    D('data_role')->where(['jobnumber'=>$teacher_jobnumber])->delete();
                    D('data_bind')->where(['uid'=>$teacher_uid])->delete();
                    D('user_group_link')->where(['uid'=>$teacher_uid])->delete();
                    D('User')->where(['uid'=>$teacher_uid])->delete();
                } else {
                    $uid = D('data_role')->where(['jobnumber'=>$addData3['jobnumber'],'school_id'=>$addData3['school_id'],'classroom_id'=>$addData3['classroom_id']])->getField('uid');
                    if ($uid) {
                        $group = D('user_group_link')->where(['uid'=>$uid])->findAll();
                        $group_mark = 0;
                        if ($group) {
                            foreach ($group as $key=>$value) {
                                if (in_array('17',$value)) {
                                    $group_mark += 1;
                                } else {
                                    $group_mark += 0;
                                }
                            }
                        } else {
                            $group_mark = 0;
                        }
                    }
                    $user_login = $teachers[0]['name'].$addData3['jobnumber'];
                    $user_id = D('user')->where("login='$user_login' or uname like '%$user_login%'")->getField('uid');
                    define('UID',$user_id);
                    if (D('data_role')->where(['jobnumber'=>$addData3['jobnumber'],'school_id'=>$addData3['school_id'],'classroom_id'=>$addData3['classroom_id']])->count() && $group_mark) {
                        continue;
                    }
                }
                $addData3['xingming'] = $value['name'];
                $addData3['en_xingming'] = $value['engname'];
                $addData3['sex'] = $value['性别'] == "男" ? 1 : 2;

                //user_group_link表中没有17[老师组]权限也添加
                $uid = D('data_role')->where(['jobnumber'=>$value['员工工号']])->getField('uid');
                if (!$group_mark || !$uid) {
                    $uid = $this->add_user($addData3,1);
                    if ($uid < 0) {
                        $uid = $teacher_uid;
                    }
                    if ($uid < 0) {
                        $log3['status'] = 0;
                        $log3['des'] = '老师,工号:'.$value['员工工号'].'写入ts_user表失败';
                        continue;
                    } else {
                        $statistics[] = $uid;
                    }
                }
                $addData3['uid'] = empty($uid) ? UID :$uid;
                if ($value['职务'] == '主教')
                    $addData3['level'] = 1;
                else
                    $addData3['level'] = 2;
                $addData3['mtime'] = time();
                $addData3['mdate'] = date('Y-m-d H:i:s',$addData3['mtime']);
                if ($res = D('data_role')->add($addData3)) {
                    $log3['status'] = 1;
                    $log3['des'] = 'role_id: '.$res.',老师,工号:'.$value['员工工号'].'班级ID:“'.$addData3['classroom_id'].'”添加成功';
                    $bind['type'] = 3;
                    $bind['type_id'] = $addData3['classroom_id'];
                    $bind['uid'] = empty($uid) ? UID : $uid;
                    $bind['ctime'] = time();
                    D('data_bind')->add($bind);
                } else {
                    $log3['status'] = 0;
                    $log3['des'] = '老师，工号:“'.$value['员工工号'].'”,班级ID:“'.$addData3['classroom_id'].'”写入ts_data_role表失败';
                }
                D('data_sync_log')->add($log3);
            }
            $time = time();
            var_dump('finish teacher refresh');
        } else {
            var_dump('no teacher refresh');
        }
        unset($teachers);
        unset($log3);
        unset($bind);
        unset($addData3);
        //加入统计
        $sta['cdate'] = $now_date;
        if(!$sid = D('data_statistics')->where($sta)->getFiled('sid'))
            $sid = D('data_statistics')->add($sta);
        $stat['teacher_count'] = count($statistics);
        $stat['teacher_uids'] = implode(',',$statistics);
        D('data_statistics')->where($sta)->save($stat);
    }
    private function add_user($info,$type=0)
    {
        $user['uname'] = $info['xinfgming'].$info['jobnumber'];
        //老师员工,通过用户名判断是否已经注册
        if (in_array($type,[1,2])) {
            $uid = model('User')->where(['login'=>$user['uname']])->getField('uid');
            if ($uid) {
                $group = D('user_group_link')->where(['uid'=>$uid])->findAll();
                $role_mark = 1;
                if ($group) {
                    foreach ($group as $key=>$value) {
                        if (in_array('17',$value)) {
                            $group_mark = true;
                        } else {
                            $group_mark = false;
                        }
                    }
                } else {
                    $group_mark = false;
                }
            }
            if ($uid && $group_mark) {
                if ($type == 2) {
                    return -1;
                } else {
                    return $uid;
                }
            }
            $user['login'] = $user['uname'];
        } else {
            //学员与准学员通过手机号判断是否已经注册
            if ($uid = model('User')->where(['login' => $info['tel']])->getField('uid')) {
                if ($info['school_id']) {
                    $detail = D('data_school')->where('school_id=' . $info['school_id'])->find();
                    $user['city'] = $detail['city_id'];
                    $user['location'] = $detail['city_name'];
                    $user['school_id'] = $detail['school_id'];
                } else {
                    if ($info['city_id']) {
                        $user['city'] = $info['city_id'];
                        $user['location'] = D('data_city')->where('city_id=' . $info['city_id'])->getField('city_name');
                    } else {
                        $user['city'] = 1001;
                        $user['location'] = '北京';
                    }
                }
                model('User')->where('uid=' . $uid)->save($user);
                return $uid;
            } else {
                return 0;
            }
        }
        $user['password'] = 123456;
        $user['login_salt'] = rand(1111,9999);
        $user['ctime'] = time();
        $user['reg_ip'] = get_client_ip();
        $user['password'] = md5(md5('123456').$user['login_salt']);
        $user['sex'] = $info['sex'];
        $user['is_adult'] = 1;
        $user['is_active'] = 1;
        $user['is_init'] = 0;

        //添加昵称拼音索引
        $user['first_letter'] = getFirstLetter($user['uname']);
        //如果包含中文,将中文翻译成拼音
        if (preg_match('/[\x7f-\xff]+/',$user['uname'])) {
            //昵称和昵称拼音保存到搜索字段
            $user['search_key'] = $user['uname'] . ' ' . model('PinYin')->Pinyin($user['uname']);
        } else {
            $user['search_key'] = $user['uname'];
        }
        if ($info['school_id']) {
            $detail = D('data_school')->where('school_id='.$info['school_id'])->find();
            $user['city'] = $detail['city_id'];
            $user['location'] = $detail['city_name'];
            $user['school_id'] = $info['school_id'];
        } else {
            if ($info['city_id']) {
                $user['city'] = $info['city_id'];
                $user['location'] = D('data_city')->where('city_id='.$info['city_id'])->getField('city_name');
            } else {
                $user['city'] = 1001;
                $user['location'] = '北京';
            }
        }
        //user表和group_link表无数据时,添加用户
        if (!$uid && !$group_mark) {
            $uid = model('User')->add($user);
        }
        if ($uid) {
            $regdata = $this->reg_config;
            //添加到默认用户组
            $userGroup = empty($regdata['default_user_group']) ? C('DEFAULT_GROUP_ID') : $regdata['default_user_group'];
            if ($type == 1) {
                $userGroup[] = 17;//老师组
            }
            if ($type == 2)
                $userGroup[] = 10;//内部员工组
            if ($type == 3)
                $userGroup[] = 19;//学员组
            model('UserGroupLink')->domoveUsergroup($uid, implode(',', $userGroup));
            //添加双向关注用户
            $eachFollow = $regdata['each_follow'];
            if (!empty($eachFollow)) {
                model('Follow')->eachDoFollow($uid, $eachFollow);
            }
            //添加默认关注用户
            $defaultFollow = $regdata['default_follow'];
            $defaultFollow = array_diff(explode(',', $defaultFollow), explode(',', $eachFollow));
            if (!empty($defaultFollow))
                model('Follow')->bulkDoFollow($uid, $defaultFollow);
            if ($role_mark)
                return 0;
            else
                return $uid;
        } else {
            return 0;
        }

    }
    public function employee()
    {
        set_time_limit(0);
        $db = new SimpleDB($this->connection);
        $now_date = date('Y-m-d',strtotime("-1 day"));

        //统计
        $statistics = array();
        $log['type'] = 0;
        $log['ctime'] = $now_date;
        $log['status'] = 1;
        $log['des'] = 'employee已经执行';
        D('data_sync_log')->add($log);
        //内部员工
        $employees = $db->query("SELECT * FROM employee WHERE hrnum is not nul order by _MASK_TO_V2 asc");
        if (count($employees)) {
            foreach ($employees as $key=>$value) {
                if (!in_array($value['status'],array(001,002,003,004,013))){
                    $employees_uid = D('data_role')->where(['jobnumber'=>$value['hrnum']])->getField('uid');
                    if ($employees_uid) {
                        D('data_role')->where(['uid'=>$employees_uid])->delete();//删除员工表的记录
                        D('data_bind')->where(['uid'=>$employees_uid])->delete();//删除绑定表的记录
                        D('user_group_link')->where(['uid'=>$employees_uid,'user_group_id'=>array('in',array(10,17))])->delete();
                        $log4['status'] = 0;
                        $log4['des'] = '离职员工,工号:'.$value['hrnum'].'已处理';
                        D('data_sync_log')->add($log4);
                    }
                    continue;
                }
                if (D('data_role')->where(['jobnumber'=>$value['hrnum']])->getField('uid')) {
                    continue;
                }
                $log4['type'] = 4;
                $log4['ctime'] = $now_date;
                $log4['_MASK_TO_V2'] = $value['_MASK_TO_V2'];
                $addData4['jobnumber'] = $value['hrnum'];
                if (stristr($value['hrnum'],'wx')) {
                    $addData4['city_id'] = D('data_city')->where(['city_name'=>'无锡'])->getField('city_id');
                }else if(stristr($value['hrnum'], 'sz')){
                    $addData4['city_id'] = D('data_city')->where(array('city_name'=>'深圳'))->getField('city_id');   //深圳
                }else if(stristr($value['hrnum'], 'sh')){
                    $addData4['city_id'] = D('data_city')->where(array('city_name'=>'上海'))->getField('city_id');   //上海
                }else if(preg_match("/^p[0-9]{2,}$/", $value['hrnum'], $matches) !== 0){
                    $addData4['city_id'] = D('data_city')->where(array('city_name'=>'杭州'))->getField('city_id');   //杭州
                }else if(stristr($value['hrnum'], 'gz')){
                    $addData4['city_id'] = D('data_city')->where(array('city_name'=>'广州'))->getField('city_id');   //广州
                }else{
                    $addData4['city_id'] = D('data_city')->where(array('city_name'=>'北京'))->getField('city_id');   //北京
                }
                $addData4['xingming'] = $value['name'];
                $addData4['en_xingming'] = $value['engname'];
                $addData4['sex'] = $value['sex']?$value['sex']:1;
                //添加用户
                $uid = $this->add_user($addData4,2);
                if($uid<0){
                    $log4['status'] = 0;
                    $log4['des'] = '内部员工，工号:“'.$value['hrnum'].'”,写入ts_user表失败';
                    D('data_sync_log')->add($log4);
                    continue;
                }else {
                    // 加入到统计
                    $statistics[] = $uid;
                }
                $addData4['uid'] = $uid;
                $addData4['mtime'] = time();
                $addData4['mdate'] = date('Y-m-d H:i:s',$addData4['mtime']);
                if(D('data_role')->add($addData4)){
                    $log4['status'] = 1;
                    $log4['des'] = '内部员工，工号:“'.$v4['hrnum'].'”添加成功';
                }else{
                    $log4['status'] = 0;
                    $log4['des'] = '内部员工，工号:“'.$v4['hrnum'].'”写入ts_data_role表失败';
                }
                D('data_sync_log')->add($log4);
            }
            var_dump('finish employee refresh');
        } else {
            var_dump("no employee refresh");
        }
        //加入统计
        $sta['cdate'] = $now_date;
        if( !$sid = D('data_statistics')->where($sta)->getField('sid') ){
            $sid = D('data_statistics')->add($sta);
        }
        $stat['employee_count'] = count($statistics);
        $stat['employee_uids'] = implode(',', $statistics);
        D('data_statistics')->where($sta)->save($stat);
    }
}