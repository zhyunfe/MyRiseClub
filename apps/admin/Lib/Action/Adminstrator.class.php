<?php
/**
 * 后台框架基类
 * Created by PhpStorm.
 * User: Demoer
 * Date: 17/5/31
 * Time: 下午12:29
 */
class AdminstratorAction extends Action {
    //页面字段列表
    protected $pageKeyList = array();
    //针对搜索 或者 页面字段的额外属性
    protected $opt = array();
    //搜索的字段
    protected $searchKey = array();
    //页面字段配置在system_data表中的页面唯一的key值
    protected $pageKey = '';
    //页面搜索配置存在system_data表中的页面唯一key值
    protected $searchPagekey = '';
    //默认的配置页面保存地址
    protected $savePostUrl = '';
    //搜索提交地址
    protected $searchPostUrl = '';
    //配置页面的值在ststem_data表中的对应list值
    protected $systemdata_list = '';
    //配置页面的值在system_data表中对应的key值
    protected $systemdata_key = '';
    //列表页的TAB 切换项
    protected $pageTab = array();
    //列表页在分页栏的按钮
    protected $pageButton = array();
    //列表页是否有全选项
    protected $allSelected = true;
    //列表中的主键字段
    protected $_listpk = 'id';
    //页面载入时需要执行的JS列表 (直接函数名)
    protected $onload = array();
    //提交时需要进行验证的js函数
    protected $onsubmit = '';
    //不能为空的字段
    protected $notEmpty = array();
    protected $navList = array();
    protected $submitAlias = '保存';

    public function _initialize()
    {
        if (!model('Passport')->checkAdminLogin()) {
            redirect(U('admin/Public/login'));
        }
    }

}