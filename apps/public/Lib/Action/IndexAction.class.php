<?php

/**
 * Created by PhpStorm.
 * User: changwei
 * Date: 17/6/14
 * Time: 上午10:03
 */
class IndexAction extends Action
{
    /**
     * 我的首页  -  微博页面
     * @return void
     */
    public function index()
    {

    }
    //没有初始化
    public function loginWithoutInit()
    {

    }
    //没有登录
     public function nologin()
     {

     }

    /**
     * 我的微博页面
     */
    public function myFeed()
    {

    }

    /**
     * 我的关注页面
     */
    public function following()
    {

    }

    /**
     * 我的粉丝页面
     */
    public function follower()
    {

    }

    /**
     * 意见反馈页面
     */
    public function feedback()
    {

    }

    /**
     * 获取验证码图片操作
     */
    public function verify()
    {

    }

    /**
     * 获取指定用户小名片所需的数据
     */
    public function showFaceCard()
    {

    }

    /**
     * 公告详细页面
     */
    public function announcement()
    {

    }

    /**
     * 公告列表页面
     */
    public function announcementList()
    {

    }

    /**
     * 自动提取标签操作
     */
    public function getTags()
    {

    }

    /**
     * 根据指定应用和表获取指定用的标签,同个人空间中用户标签
     * @param $uids
     */
    private function _assignUserTag($uids)
    {

    }

    /**
     * 弹窗发微博
     */
    public function sendFeedBox()
    {

    }
    /**
     * 显示积分细节
     */
    public function scoredetail()
    {

    }

    /**
     * 新加 删除广告
     */
    public function delAd()
    {

    }
    public function test()
    {

    }
    public function ajaxShareNet()
    {

    }

    /**
     * 第一次登陆默认推荐好友
     */
    public function registerRecommend()
    {

    }

    /**
     * 每日更新城市\学校\班级
     */
    public function daily_sync_city_school_class()
    {
        $starttime = $_GET['starttime'];
        model('Sync')->city_school_class($starttime);
    }

    /**
     * 每日更新教师
     */
    public function daily_sync_teacher()
    {
        $jobnumber = $_GET['jobnumber'];
        model('Sync')->teacher($jobnumber);
    }

    /**
     * 每日更新员工
     */
    public function daily_sync_employee()
    {
        model('Sync')->employee();
    }

    /**
     * 每日更新学习规划师
     */
    public function daily_sync_learning_planner()
    {

    }

    /**
     * 每日更新学生
     */
    public function daily_sync_student()
    {

    }

    /**
     * 每日更新准学员
     */
    public function daily_sync_zhun_student()
    {

    }

    /**
     *
     */
    public function getScoremark()
    {

    }
}